//
//  SceneDelegate.h
//  CITest
//
//  Created by 任一杰 on 2020/9/24.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

